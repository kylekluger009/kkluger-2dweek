 using UnityEngine;
 using System.Collections;
 
 public class enemyAI : MonoBehaviour {

     //https://answers.unity.com/questions/754633/how-to-move-an-object-left-and-righ-looping.html
 
     public float delta = 1.5f;  // Amount to move left and right from the start point
     public float speed = 2.0f; 
     private Vector2 startPos;
 
     void Start () {
         startPos = transform.position;
     }
     
     void Update () {
         Vector2 v = startPos;
         v.x += delta * Mathf.Sin (Time.time * speed);
         transform.position = v;
     }
 }