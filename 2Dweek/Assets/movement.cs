using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class movement : MonoBehaviour
{

    public CharacterController2D controller;

    public float runSpeed = 40f;

    public float horizontalMove = 0f;

    bool jump = false;

    public TextMeshProUGUI countText;
    public int count;


    // Start is called before the first frame update


    void Start ()
    {
        count = 0;
        SetCountText();

    }
    void Update()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;

        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false;
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject.CompareTag ("Pickup"))
	    {
            Debug.Log("Yes");
			other.gameObject.SetActive (false);

			// Add one to the score variable 'count'
			
            count = count + 1;
			// Run the 'SetCountText()' function (see below)
			
		    SetCountText();
        }

        if (other.gameObject.CompareTag ("enemy"))
	    {
			other.gameObject.SetActive (false);

			// Add one to the score variable 'count'

            countText.text = "You killed HIM!!";
            Application.Quit();

          
			
			// Run the 'SetCountText()' function (see below)
			
		    
        }


		
    }

    void SetCountText()
	{
		countText.text = "Count: " + count.ToString();
        // if (count > 0) 
		// {
        //     countText.text = "You Win!";
        // }
	}
    
}
